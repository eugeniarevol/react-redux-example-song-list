import React, { Component, Fragment } from 'react';
import SongList from './SongList';
import SongDetail from './SongDetail';
import Navbar from './Navbar';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Navbar />
        <div className="container">
          <div className="row mt-5">
            <div className="col-sm"><SongList /></div>
            <div className="col-sm"><SongDetail /></div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;

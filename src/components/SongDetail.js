import React, { Component } from 'react';
import { connect } from 'react-redux';

class SongDetail extends Component {
  render() {
    const { song } = this.props;
    if (!song) {
      return <h1>Select a song</h1>
    }
    return (
      <div className="card bg-secondary mb-3">
        <div className="card-header">Selected Song</div>
        <div className="card-body">
          <h4 className="card-title">{song.title}</h4>
          <p className="card-text">{song.duration}</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { song: state.selectedSong }
}

export default connect(
  mapStateToProps
)(SongDetail);
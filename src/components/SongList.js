import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectSong } from '../actions';

class SongList extends Component {
  renderList() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Title</th>
            <th scope="col">Duration</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {this.props.songs.map((song,index) =>
            <tr key={song.title} className="table-light">
              <th scope="row">{index + 1}</th>
              <td>{song.title}</td>
              <td>{song.duration}</td>
              <td>
                <button type="button"
                  className="btn btn-primary"
                  onClick={() => this.props.selectSong(song)}
                >Select</button>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    )
  }
  render() {
    return (
      <div>{this.renderList()}</div>
    );
  }
}

// by convention this function is called mapStatetoProps
//but could be called with another name
//if you inspect SongList component now you can see
//the state map to its props
const mapStateToProps = (state) => {
  return { songs: state.songs };
}

// this is a javascript function called
//connect calling inside the return another
// function in this case the SongList component
export default connect(
  mapStateToProps,
  { selectSong }
)(SongList);
import {combineReducers} from 'redux';

const songsReducer = () => {
  return [
    {title:'Nirvana - Smells Like Teen Spirit', duration:'4:05'},
    {title:'U2 - One', duration:'3:35'},
    {title:'Backstreet Boys - I Want It That Way', duration:'3:25'},
    {title:'Whitney Houston - I Will Always Love You', duration:'3:15'},
    {title:'Madonna - Vogue', duration:'3:03'},
    {title:'Sir Mix-A-Lot - Baby Got Back', duration:'3:12'},
    {title:'Britney Spears - Baby One More Time', duration:'3:11'},
    {title:'R.E.M. - Losing My Religion', duration:'3:36'},
    {title:'Alanis Morissette - You Oughta Know', duration:'3:33'},
    {title:'Red Hot Chili Peppers - Under the Bridge', duration:'3:43'},
    {title:'MC Hammer - U Cant Touch This', duration:'3:03'},
  ];
}

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') {
    return action.payload;
  }

  return selectedSong;
}

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
})